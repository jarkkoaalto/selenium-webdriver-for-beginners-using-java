package section03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingBrowser {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		// Storing the Application URL in the string value
		String url = "https://www.eurail.com/en";
		driver.get(url);
		
		driver.manage().window().maximize();
		
		// Storing title name in  the string variable
		String actualTitle = driver.getTitle();
		// System.out.println(actualTitle);
		
		// Storing Expected title in String variable
		String expectedTitle = "Discover Europe by Train with Eurail | Eurail.com";
		
		// Verification
		if(actualTitle.equals(expectedTitle))
		{
			System.out.println("The Title of the page is : " + actualTitle);
		}
		else{
			System.out.println("Title varification is failed");
			System.out.println("Actual title name is " + actualTitle);
			System.out.println("Expected Title name is " + expectedTitle);
		}
		
		// Navigate to help screen
		driver.findElement(By.linkText("HELP")).click();
		String actualURL = driver.getCurrentUrl();
		
		String expectedURL = "https://www.eurail.com/en/help";
		
		if(actualURL.equals(expectedURL))
		{
			System.out.println("The current URL of the page is : " + actualURL);
		}else {
			System.out.println("Title varification is failed");
			System.out.println("Actual title name is " + actualURL);
			System.out.println("Expected Title name is " + expectedURL);
		}
		
		// Storing Page Source in String variable
		String pageSource = driver.getPageSource();
		
		// Storing Page source lenght in Int variable
		int pageSourceLength = pageSource.length();
		
		// Printing length of the page source on console
		System.out.println("Total length of the Page Source is :" + pageSourceLength);
		
		// One more page for using Quit method
		driver.get("https://www.eurail.com/en/book-your-reservations-with-eurail");
		
		driver.quit();
		//driver.close();
	}

}
