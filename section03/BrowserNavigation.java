package section03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserNavigation {

	public static void main(String[] args) throws InterruptedException {
	
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		// open web site
		String appUrl = "https://www.amazon.com/";
		driver.get(appUrl);
		
		Thread.sleep(3000);
		
		// Click on cars link
		driver.findElement(By.xpath("//*[@id=\"nav-cart\"]/span[2]")).click();
		
		Thread.sleep(3000);
		// go back to previous page
		driver.navigate().back();
		
		Thread.sleep(3000);
		
		// navigate to forward page
		driver.navigate().forward();
		
		Thread.sleep(3000);
		// Refresh the browser
		driver.navigate().refresh();
		Thread.sleep(3000);
		// navigate to customer service page
		driver.navigate().to("https://www.amazon.com/gp/help/customer/display.html?nodeId=508510&ref=nav_cs_help");
		
		driver.close();
		
		
	}

}
