package section05;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HandleCheckBox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Jarkko\\Selenium\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();

		// Open web site
		String appUrl = "http://demoqa.com/registration/";
		driver.get(appUrl);
		WebElement cricketCheckbox = driver.findElement(By.xpath("html/body/div[1]/div/div[1]/main/article/div/div/div[3]/from/ul/li[3]/div/div[1]/input[3]"));
		
		// Verify the Checkbox is displayed
				if(cricketCheckbox.isDisplayed())
				{
					System.out.println("Checkbox is displayed");
				}else {
					System.out.println("Checkbox is not dispalyed");
				}
				
				Thread.sleep(3000);
				if(cricketCheckbox.isEnabled()) {
					System.out.println("Checkbox is Enabled");	
				}else {
					System.out.println("Checkbox is Disabled");
				}
				
				Thread.sleep(3000);
				if(cricketCheckbox.isSelected())
				{
					System.out.println("Criclet Checkbox is Selected");
				}
				else {
					System.out.println("Criclet Checkbox is not Selected");
					cricketCheckbox.click();
				}
				
				Thread.sleep(3000);
				// Verify the Cricket Checkbox is selected or not
				if(cricketCheckbox.isSelected())
				{
					System.out.println("Criclet Checkbox is Selected");
				}
				else {
					System.out.println("Criclet Checkbox is not Selected");
				}
				
				Thread.sleep(3000);
				// Unchecked the selected value
				if(cricketCheckbox.isSelected())
				{
					cricketCheckbox.click();
					System.out.println("Cricket Checkbox is Unchecked");
				}else {
					System.out.println("Unable to uncheck the Cricket Checkbox");
				}
				
				driver.close();
		
	}

}
