package section05;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HandleRadioButton {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Jarkko\\Selenium\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();

		//Open web site
		String appUrl = "https://www.facebook.com/";
		driver.get(appUrl);
				
		WebElement submitRadioButton = driver.findElement(By.id("u_0_a"));

		// Verify the Round Trip Radio Button is displayed
		if(submitRadioButton.isDisplayed())
		{
			System.out.println("RadioButton is displayed");
		}else {
			System.out.println("RadioButton is not dispalyed");
		}
		if(submitRadioButton.isEnabled()) {
			System.out.println("RadioButton is Enabled");
		}else {
			System.out.println("RadioButton is Disabled");
		}
		Thread.sleep(3000);
		if(submitRadioButton.isSelected())
		{
			System.out.println("Criclet Checkbox is Selected");
		}
		else {
			System.out.println("Criclet Checkbox is not Selected");
		}
		// Select the RadioButton
		submitRadioButton.click();
		
		// Verify the RadioCheckbox is selected or not
		if(submitRadioButton.isSelected())
		{
			System.out.println("Criclet Checkbox is Selected");
		}
		else {
			System.out.println("Criclet Checkbox is not Selected");
		}
		driver.close();
	}

}
