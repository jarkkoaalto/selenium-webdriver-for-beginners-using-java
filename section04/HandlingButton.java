package section04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HandlingButton {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Jarkko\\Selenium\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		
		//Open web site
		String appUrl = "https://www.facebook.com/";
		driver.get(appUrl);
		
		WebElement submitButton = driver.findElement(By.name("websubmit"));
		
		// Verify the submit button is displayed
		if(submitButton.isDisplayed())
		{
			System.out.println("Submit Button is displayed");
		}else {
			System.out.println("Submit Button is not dispalyed");
		}
		if(submitButton.isEnabled()) {
			System.out.println("Submit Button is Enabled");
			// CLick on submit button
			submitButton.click();
		}else {
			System.out.println("Submit Button is Disabled");
		}
			
		driver.close();
	}

}
