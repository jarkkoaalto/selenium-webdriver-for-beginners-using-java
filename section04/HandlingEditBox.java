package section04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingEditBox {

	public static void main(String[] args) throws InterruptedException {
		//Create a new instance of the Chrome driver
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		
		// Open target web site
		String appUrl = "https://www.facebook.com/";
		driver.get(appUrl);
		
		WebElement firstName = driver.findElement(By.name("firstname"));
		
		// verify the First name is displayed
		if(firstName.isDisplayed())
		{
			System.out.println("First name is displayed");
		}else
		{
			System.out.println("First name is not displayed");
		}
		// Verify the first name is enabled
		if(firstName.isEnabled())
		{
			System.out.println("First name is enabled");
		}else {
			System.out.println("FIrst name is disabled");
		}
		
		// Enter the first name
		if(firstName.isEnabled())
		{
			firstName.sendKeys("John");
			System.out.println("First name is entered");
		}else {
			System.out.println("Unable to enter the First name");
		}
		
		// Get the value from the first name edit box
		// String fneditbox = firstName.getAttribute("");
		// System.out.println("The value presented in the first name field id " + fneditbox);
		 // Clear the value in first name field
		Thread.sleep(3000);
		firstName.clear();
		
		// Get the value from the first name edit box
		/// String fneditbox02 = firstName.getAttribute("");
		/// System.out.println("The value presented in the first name field id " + fneditbox02);
		driver.close();
	}

}
