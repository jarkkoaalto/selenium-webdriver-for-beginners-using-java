package section06;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HandleImage {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver","C:\\Users\\Jarkko\\Selenium\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();

		//Open web site
		String appUrl = "http://newtours.demoaut.com/";
		driver.get(appUrl);
		
		WebElement mercuryImage = driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/p[1]/img"));
		
		// Verify the Mercury Image is disabled or not
		if(mercuryImage.isDisplayed()) {
			System.out.println("Image is dispalyed");
			// verify the image text value
			System.out.println("The image text is " + mercuryImage.getAttribute("alt"));
		}else {
			System.out.println("Image is not displayed");
		}
		
		WebElement SignIn = driver.findElement(By.name("login"));
		if(SignIn.isEnabled()) {
			System.out.println("Login in enabled");
			// Click on Sign-in image
			SignIn.click();
			System.out.println("Login is clicked");
		}else {
			System.out.println("Login is not enabled");
		}
		
		// Navigate to Oracle page
		driver.navigate().to("https://www.oracle.com//technetwork/java/javase/downloads/jdk11-downloads-5066655.html");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"Wrapper_FixedWidth_Centercontent\"]/div[1]/div[3]/div/table/tbody/tr/td[1]/div/a/img"));
		System.out.println("Java Download Image Link Is Clicked");
		driver.close();
	}
	
	
}
