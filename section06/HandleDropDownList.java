package section06;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class HandleDropDownList {

		public static void main(String[] args) throws InterruptedException {
			//Create a new instance of the Chrome driver
			System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
		
			driver.manage().window().maximize();
			
			// Open target web site
			String appUrl = "https://www.facebook.com/";
			driver.get(appUrl);
			
			WebElement birthdayDropBox = driver.findElement(By.id("day"));
			
			//Verify the Day Drop Down List is displayed
			if(birthdayDropBox.isDisplayed())
			{
				System.out.println("Day DropDown is displayed");	
			}else {
				System.out.println("Day DropDown is not displayed");
			}
			
			//Verify the Day Drop Dwon list enabled
			if(birthdayDropBox.isEnabled()) {
				System.out.println("Day Dropdown is enabled");
			}else {
				System.out.println("Day Drop Dwon is not enabled");
			}
			
			// Creating the select object
			Select dropDown = new Select(birthdayDropBox);
			//dropDown.selectByVisibleText("13");
			//deopDown.selectByValue("13");
			dropDown.selectByIndex(13);
			
			driver.close();
		}

}
