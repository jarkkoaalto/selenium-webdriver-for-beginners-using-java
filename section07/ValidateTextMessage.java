package section07;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ValidateTextMessage {

	public static void main(String[] args) throws InterruptedException {
		//Create a new instance of the Chrome driver
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		
		// Open target web site
		String appUrl = "https://www.facebook.com/";
		driver.get(appUrl);
		Thread.sleep(2000);
		WebElement titleValidation = driver.findElement(By.xpath("//*[@id=\"content\"]/div/div/div/div/div[2]/div[1]/div[1]"));
		
		//Verify the registery is dispaly
		if(titleValidation.isDisplayed()) {
			System.out.println("Title is displayed");
		}else {
			System.out.println("Title is not displayed");
		}
		
		//validationg the Actual and expected text
		String expectedText ="Rekisteröidy";
		String actualText = titleValidation.getText();
		
		// Comparing the text
		if(actualText.equals(expectedText))
		{
			System.out.println("The actual and expected result are same. The title value is: " + actualText);
		}
		else {
			System.out.println("The actual and expected result are not same");
		}
	
		WebElement submitButton = driver.findElement(By.name("websubmit"));
		
		// Verify the submit button is displayed
		if(submitButton.isDisplayed())
		{
			System.out.println("Submit Button is displayed");
		}else {
			System.out.println("Submit Button is not dispalyed");
		}
		if(submitButton.isEnabled()) {
			System.out.println("Submit Button is Enabled");
			// CLick on submit button
			submitButton.click();
		}else {
			System.out.println("Submit Button is Disabled");
		}
		
		WebElement titleErrorValidation = driver.findElement(By.xpath("//*[@id=\"u_0_10\"]/i[1]"));
		//Verify the registery is dispaly
		if(titleErrorValidation.isDisplayed()) {
			System.out.println("Error Title is displayed" );
		}else {
			System.out.println(" Error Title is not displayed");
		}
		driver.close();		
	}

}
