package section02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorsExample {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Jarkko\\Desktop\\Udemy\\GITREPOS\\selenium-webdriver-for-beginners-using-java\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://newtours.demoaut.com/mercuryregister.php");
		driver.manage().window().maximize();
		
		// Examples of Name Locator
		driver.findElement(By.name("firstName")).sendKeys("Harry");
		driver.findElement(By.name("lastName")).sendKeys("Potter");
		driver.findElement(By.name("phone")).sendKeys("0401234567");
		driver.findElement(By.id("userName")).sendKeys("harry.potter@potter.com");
		
		driver.findElement(By.name("address1")).sendKeys("MagicStreet 23 A 12");
		driver.findElement(By.name("city")).sendKeys("Hogwats");
		driver.findElement(By.name("state")).sendKeys("ML");
		driver.findElement(By.name("postalCode")).sendKeys("3/4");
		
		// Example of ID Location
		driver.findElement(By.id("email")).sendKeys("harry.potter@potter.com");
		
		driver.findElement(By.name("password")).sendKeys("gflkg240o2+q");
		driver.findElement(By.name("confirmPassword")).sendKeys("gflkg240o2+q");
		
		// Example of link locator
		driver.findElement(By.linkText("SIGN-ON")).click();
		
		// Example of Partial linkText
		driver.findElement(By.partialLinkText("Des")).click();

	}

}
