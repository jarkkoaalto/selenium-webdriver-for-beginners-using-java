package section02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathExample {
	public static void main(String [] args) {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://raileurope-world.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//*[@id=\"rtab_content_tab1\"]/div/div/div/form/fieldset[1]/div[1]/div[1]/input[1]")).sendKeys("Brussels");
		driver.findElement(By.xpath("//*[@id=\"rtab_content_tab1\"]/div/div/div/form/fieldset[1]/div[1]/div[3]/input")).sendKeys("Paris");
		driver.findElement(By.xpath("//*[@id=\"rtab_content_tab1\"]/div/div/div/form/div[1]/input")).click();
	}
}
