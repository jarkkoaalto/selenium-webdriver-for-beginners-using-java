package section02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Locators {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Jarkko\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		// Launch and maximize the application
		driver.get("https://www.google.com");
		driver.manage().window().maximize();
		
		// Example of CSS Selector
		driver.findElement(By.tagName("Kokeilen onneani")).click();
		//driver.findElement(By.name("Google-haku")).click();
		
	}

}
